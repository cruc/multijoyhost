#include "stdafx.h"

#include "Serial.h"

#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <sstream>
#include <set>
#include <vector>

using namespace std;

// To transmit up to 3*16 = 48 different channels to Dosbox, we need 48 different keycodes that will be understood by DOS
vector<DWORD> useableKeyCodes = {
	48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // The digits from 0-9
	65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, // The letters from A TO Z
	96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107 // Digits and Multiply+Add on the num block
};

int SendKey(WORD virtualKeyCode, bool sendUp = false) {
	KEYBDINPUT keyboardInput;
	ZeroMemory(&keyboardInput, sizeof(keyboardInput));

	keyboardInput.wVk = virtualKeyCode;
	keyboardInput.dwExtraInfo = GetMessageExtraInfo();

	if (sendUp) {
		// If the key is not down, but up, we need to specify a flag
		keyboardInput.dwFlags = KEYEVENTF_KEYUP;
	}

	INPUT inputStruct;
	ZeroMemory(&inputStruct, sizeof(inputStruct));
	inputStruct.ki = keyboardInput;
	inputStruct.type = 1;
	return SendInput(1, &inputStruct, sizeof(INPUT));
}

void multiJoyToKeyPress(const char *dataString, set<byte> &down) {
	down.clear();
	unsigned data[16];
	int items = sscanf(dataString, " %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x\n",
		&data[0], &data[1], &data[2], &data[3], &data[4], &data[5], &data[6], &data[7],
		&data[8], &data[9], &data[10], &data[11], &data[12], &data[13], &data[14], &data[15]);
	if (items == 16) {
		for (int address = 0; address < 16; address++) {
			// Read joystick bit from array
			if (data[address] & (1 << 5)) {
				down.insert(useableKeyCodes.at(address)); 
			}
			if (data[address] & (1 << 7)) {
				down.insert(useableKeyCodes.at(address + 16)); 
			}
		}
	}
	else {
		cerr << "Failed to parse reply from Multijoy interface: " << items << " read from " << dataString << endl;
	}
}

bool extract_next_line(string &buf, string &outLine)
{
	string::size_type pos;
	if ((pos = buf.find('\n')) != string::npos)
	{
		outLine = buf.substr(0, pos);
		buf.erase(0, pos + 1);
		return true;
	}
	else {
		return false;
	}
}


char *port_name = "\\\\.\\COM6";

int main() {
	char incomingData[MAX_DATA_LENGTH];

	SerialPort arduino(port_name);
	if (arduino.isConnected()) {
		cout << "Connection Established" << endl;
	}
	else {
		cout << "ERROR, check port name";
		return -1;
	}

	set<byte> down;
	set<byte> up;
	string stream;

	while (arduino.isConnected()) {
		int read_result = arduino.readSerialPort(incomingData, MAX_DATA_LENGTH);
		if (read_result > 0) {
			incomingData[read_result] = '\0';
			// Write result into stream
			stream += incomingData;

			// Now exhaust the stream - due to serial com buffering, there might be multiple lines in the buffer
			string line;
			while (extract_next_line(stream, line)) {
				cout << line  << endl;
				// Successfully read a line
				multiJoyToKeyPress(line.c_str(), down);
				for (auto d : down) {
					if (up.find(d) == up.end()) {
						cout << "Sending key " << d << endl;
						SendKey(d);
					}
				}

				// Check if any keys in up are no longer in down, then we need to send a key up event
				for (auto u : up) {
					if (down.find(u) == down.end()) {
						cout << "upping key:" << u << endl;
						SendKey(u, true);
					}
				}

				up = down;
			} 
		}
	}
	return 0;
}
